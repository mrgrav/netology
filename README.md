## Проверка выполнения

    vagrant@vagrant:~$ sudo docker pull registry.gitlab.com/mrgrav/netology/python-api:latest
    latest: Pulling from mrgrav/netology/python-api
    2d473b07cdd5: Already exists
    e0e4c6ef31a3: Pull complete
    fa66c940d192: Pull complete
    f4be0a2a4b63: Extracting [===============================================>   ]  3.211MB/3.37MB
    524b23e97dc5: Download complete
    f4be0a2a4b63: Pull complete
    524b23e97dc5: Pull complete
    Digest: sha256:a6e210a5002e201c1854e7cee965b07454a5865303458a3228b339d2cab6b64f
    Status: Downloaded newer image for registry.gitlab.com/mrgrav/netology/python-api:latest
    registry.gitlab.com/mrgrav/netology/python-api:latest
    vagrant@vagrant:~$ sudo docker run -p 5290:5290 -d registry.gitlab.com/mrgrav/netology/python-api:latest
    fbd02dfbec4d2f403fc9dec27dba5d52bf9c0ccfef9c2689b7ef8517b4fe8b95
    vagrant@vagrant:~$ curl localhost:5290/get_info
    {"version": 3, "method": "GET", "message": "Running"}
    vagrant@vagrant:~$
